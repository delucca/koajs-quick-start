# KoaJS Quick Start

This repo was created to help you to start new KoaJS projects :). It is just a simple boilerplate code that any KoaJS projects need to do.

### Contents

Here a the list of the main containers in this repo:

> Container 1: APP -> KoaJS, Nodemon and TypeScript

### Getting Started and requirements

You will need Docker and Docker-Compose installed in your machine. After that, there are two methods for getting started with this repo.

#### Do you know how to use GIT?
Checkout this repo, install dependencies, doing this:

```
> git clone https://gitlab.com/delucca/koajs-quick-start.git
> cd koajs-quick-start
> sudo docker-compose build
> sudo docker-compose up
```

Afterwards Docker starts there will be a new container waiting for you:

> APP Container: You can access it on http://localhost on the host.

#### Not Familiar with Git?
Click [here](https://gitlab.com/delucca/koajs-quick-start.git) then download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
> sudo docker-compose build
> sudo docker-compose up
```
