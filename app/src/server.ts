import * as Koa from 'koa';
import * as Router from 'koa-router';

// Initialize the main constants
// -----
const app    = new Koa(),
      router = new Router();

// Apply the middlewares
// -----
app.use(async (ctx, next) => {
    // Basic middleware to log the URL
    // -----
    console.log('New visitor at: ', ctx.url);
    await next();
});

// Add the routes
// -----
router.get('/*', async(ctx) => {
    ctx.body = 'Helllo World'
});
app.use(router.routes());

// Start the server
// -----
app.listen(80)
console.log('Server running on port 80');